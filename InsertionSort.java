import java.util.*;
public class InsertionSort {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int n = input.nextInt();
		int[] arr = new int[n];
		for (int i = 0;i < n;i++) {
			arr[i] = input.nextInt();
		}
		for (int i = 1;i < n;i++) {
			int checker = arr[i];
			int j;
			for (j = i - 1;j >= 0 && checker < arr[j];j--) {
					arr[j + 1] = arr[j];
			}
			arr[j + 1] = checker;
		}
		for (int i = 0;i < n;i++) {
			System.out.print(arr[i] + " ");
		}
	}
}